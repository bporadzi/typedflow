import { Injectable } from '@angular/core';
import { Observable, combineLatest, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Repository } from 'src/app/interfaces/repository.interface';
import { switchMap, map } from 'rxjs/operators';
import { Branch } from '../interfaces/branch.interface';

@Injectable({
  providedIn: 'root'
})
export class GitApiService {

  api = 'https://api.github.com';

  constructor(
    private http: HttpClient,
  ) { }

  getUserRepositories(userName: string): Observable<Repository[]> {
    return this.http.get<ApiRepository[]>(`${this.api}/users/${userName}/repos`).pipe(
      switchMap((resp: ApiRepository[]) => {
        if (resp.length === 0) {
          return of(<Repository[]>[]);
        }

        const repositories$: Observable<Repository[]> = combineLatest(
          resp.map((repo: ApiRepository) => {
            const { branches_url, ...rest } = repo;
            const branchesListApi = branches_url.replace('{/branch}', '');
            return this.http.get<Branch[]>(branchesListApi).pipe(
              map(branches => (<Repository>{ branches, ...rest })),
            );
          }),
        );
        return repositories$;
      }),
    );
  }
}

interface ApiRepository {
  name: string;
  owner: {
    login: string;
  };
  branches_url: string;
}
