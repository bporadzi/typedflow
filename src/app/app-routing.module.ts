import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserLoginFormComponent } from './views/user-login-form/user-login-form.component';
import { AuthGuard } from './guards/auth.guard';
import { AppRoutingPath } from './app-routing-paths';
import { GitRepositoriesComponent } from './views/git-repositories/git-repositories.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: AppRoutingPath.login,
    pathMatch: 'full',
  },
  {
    path: AppRoutingPath.login,
    component: UserLoginFormComponent,
  },
  {
    path: AppRoutingPath.myGitRepositories,
    component: GitRepositoriesComponent,
    canActivate: [AuthGuard],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
