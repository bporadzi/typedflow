import { Component, Input } from '@angular/core';
import { Repository } from 'src/app/interfaces/repository.interface';
import { faAngleDown, faCodeBranch } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-repositories-table',
  templateUrl: './repositories-table.component.html',
  styleUrls: ['./repositories-table.component.scss']
})
export class RepositoriesTableComponent {

  @Input()
  repositories: Repository[];

  faAngleDown = faAngleDown;
  faCodeBranch = faCodeBranch;
}
