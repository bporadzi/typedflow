import { Branch } from './branch.interface';

export interface Repository {
    name: string;
    owner: {
        login: string;
    };
    branches: Branch[];
}
