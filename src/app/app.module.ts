import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { UserLoginFormComponent } from './views/user-login-form/user-login-form.component';
import { RepositoriesTableComponent } from './components/repositories-table/repositories-table.component';
import { GitRepositoriesComponent } from './views/git-repositories/git-repositories.component';


@NgModule({
  declarations: [
    AppComponent,
    UserLoginFormComponent,
    RepositoriesTableComponent,
    GitRepositoriesComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    FontAwesomeModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
