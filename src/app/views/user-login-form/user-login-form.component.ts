import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AppRoutingPath } from 'src/app/app-routing-paths';

@Component({
  selector: 'app-user-login-form',
  templateUrl: './user-login-form.component.html',
  styleUrls: ['./user-login-form.component.scss']
})
export class UserLoginFormComponent {

  loginControlName = 'login';

  loginForm = this.formBuilder.group({
    [this.loginControlName]: ['', Validators.required],
  });

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
  ) { }

  onSubmitClicked(): void {
    const userName = this.loginForm.get(this.loginControlName).value;
    this.router.navigate([AppRoutingPath.myGitRepositories], { queryParams: { userName } });
  }

}
