import { Component, OnInit } from '@angular/core';
import { GitApiService } from 'src/app/services/git-api.service';
import { Repository } from 'src/app/interfaces/repository.interface';
import { ActivatedRoute } from '@angular/router';
import { switchMap, catchError, tap } from 'rxjs/operators';
import { of } from 'rxjs';
import { faSpinner, faExclamationTriangle } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-git-repositories',
  templateUrl: './git-repositories.component.html',
  styleUrls: ['./git-repositories.component.scss']
})
export class GitRepositoriesComponent implements OnInit {

  repositories: Repository[];

  isLoading: boolean = true;
  isError: boolean;

  faSpinner = faSpinner;
  faExclamationTriangle = faExclamationTriangle;

  userName: string;

  constructor(
    private gitApi: GitApiService,
    private activatedRoute: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    this.activatedRoute.queryParams.pipe(
      tap(({ userName }) => this.userName = userName),
      switchMap(({ userName }: { userName: string }) =>
        this.gitApi.getUserRepositories(userName)),
      catchError(_ => {
        this.isError = true;
        return of(null);
      }),
    ).subscribe((repositories: Repository[]) => {
      this.repositories = repositories;
      this.isLoading = false;
    });
  }

}
